
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.


"""
An example of reading a line at a time from standard input
without blocking the reactor.
"""

from twisted.internet import stdio
from twisted.protocols import basic
from twisted.internet import reactor

from twisted.internet import reactor, protocol

# Protocol untuk pengiriman sama penerimaan data dari server
class EchoClient(protocol.Protocol):

	def dataReceived(self, data):
		print "Server said:", data
		print ""

	def send_message(self, msg) :
		self.transport.write(msg)

class EchoFactory(protocol.ClientFactory):
	client = None

	def buildProtocol(self, addr):
		self.client = EchoClient()
		return self.client

	def send_message(self, msg) :
		self.client.send_message(msg)

# Protocol untuk menangkap input dari keyboard
class Echo(basic.LineReceiver):
	from os import linesep as delimiter

	factory = None

	def __init__(self, factory) :
		self.factory = factory

	def connectionMade(self):
		self.transport.write('Masukkan pesan yang akan dikirim : ')

	def lineReceived(self, line):
		self.factory.send_message(line)
		self.transport.write('Masukkan pesan yang akan dikirim : ')

def main():
	factory = EchoFactory()
	# Koneksi ke TCP server
	reactor.connectTCP("localhost", 8002, factory)
	# Koneksi ke console untuk dapatkan input dari keyboard
	stdio.StandardIO(Echo(factory))	
	reactor.run()

if __name__ == '__main__':
	main()