package com.example.helloworld;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import android.os.AsyncTask;

public class InternetAsyncTask extends AsyncTask<String, Void, String>{
	InternetFragment mainFragment;
	
	public InternetAsyncTask(InternetFragment mainFragment){
		this.mainFragment = mainFragment;
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String address = params[0];
		String result = performRequest(address);
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		this.mainFragment.setResult(result);
	}
	
	public String performRequest(String address){
		//String address = "http://ip.jsontest.com/";
		String result = "";
		try {
			URL url = new URL(address);
			try {
				URLConnection connection = url.openConnection();
				HttpURLConnection httpConnection = (HttpURLConnection)connection;
				int responseCode = httpConnection.getResponseCode();
				if (responseCode == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = httpConnection.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
				    StringBuilder sb = new StringBuilder();

				    String line = null;
				    while ((line = reader.readLine()) != null)
				    {
				        sb.append(line + "\n");
				    }
				    result = sb.toString();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}
