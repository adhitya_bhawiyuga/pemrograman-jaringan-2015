package com.example.helloworld;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class InternetFragment extends Fragment {
	Button buttonInternet, buttonSocket;
	TextView textResult;
	String address = "http://ip.jsontest.com/";
	String socketAddress = "192.168.1.5";
	String socketPort = "9999";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_internet,
				container, false);
		buttonInternet = (Button) rootView.findViewById(R.id.buttonInternet);
		buttonSocket = (Button) rootView.findViewById(R.id.buttonSocket);
		textResult = (TextView) rootView.findViewById(R.id.textResult);
		buttonInternet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new InternetAsyncTask(InternetFragment.this).execute(address);
			}
		});
		buttonSocket.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new SocketAsyncTask(InternetFragment.this).execute(socketAddress, socketPort);
			}
		});
		return rootView;
	}
	
	public void setResult(String result){
		this.textResult.setText(result);
	}
}
