package com.example.helloworld;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.net.UnknownHostException;

import android.os.AsyncTask;
import android.util.Log;

public class SocketAsyncTask  extends AsyncTask<String, Void, String>{
	InternetFragment mainFragment;
	Socket socket;
	
	public SocketAsyncTask(InternetFragment mainFragment){
		this.mainFragment = mainFragment;
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String address = params[0];
		String port = params[1];
		int portNumber = Integer.parseInt(port);
		try {
			socket = new Socket(address, portNumber);
			performWrite("This is me!\r\n");
			return performRead();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		if(result != null){
			this.mainFragment.setResult(result);
		}
	}
	
	public String performRead() throws IOException{
		InputStream inputStream;		
		inputStream = socket.getInputStream();
		StringBuilder sb = new StringBuilder();
	    Log.d("Socket", "Start reading");
	    while (true) {
	    	int c = inputStream.read();
	    	if (c == '\r' || c == '\n' || c == -1) break;
	    	sb.append((char) c);
	    }
	    Log.d("Socket", "Finish reading");
	    socket.close();
	    return sb.toString();		
	}
	
	public void performWrite(String message) throws IOException{
		OutputStream out;
		
		out = socket.getOutputStream();
		Writer writer = new OutputStreamWriter(out, "UTF-8");
		writer = new BufferedWriter(writer);
		writer.write(message);
		writer.flush();
		Log.d("Socket", "Finish writing");		
	}

}
