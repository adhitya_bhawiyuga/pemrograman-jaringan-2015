import socket, sys
import select

# Port number
PORT = 6666

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.bind(('',PORT)) 
s.listen(10) 
input = [s,sys.stdin]
print 'Listening at', s.getsockname()

while True:
    try :
        inputready,outputready,exceptready = select.select(input,[],[]) 

        for conn in inputready: 
            # Handle the server socket
            if conn == s:             
                conn, address = s.accept() 
                input.append(conn) 
            # Handle standard input
            elif conn == sys.stdin:             
                junk = sys.stdin.readline() 
                running = 0 
            # Handle client socket
            else:             
                data, address = conn.recvfrom(4096)
                # Check if receive data is not empty
                try :
                    if data :
                        print 'The client says ', data
                        # Send back the message to client
                        conn.sendall('OK '+data)
                    # Empty string means connection closed
                    else :
                        conn.close() 
                        input.remove(conn)
                        print "Connection closed by client"
                except socket.error, e :
                    conn.close() 
                    input.remove(conn)
                    print "Connection closed by client"
    except KeyboardInterrupt :
        break
s.close()