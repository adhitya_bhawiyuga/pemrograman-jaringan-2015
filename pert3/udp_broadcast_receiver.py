import socket, sys

# Port number
PORT = 6666
# Maximum size of UDP packet
MAX = 65535 
# Initialize socket object with UDP/Datagram type
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Set option to broadcast
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
# Bind to a certain port
s.bind(('', PORT))
# Read incoming message
while True:
	data, address = s.recvfrom(MAX)
	print 'The client at %r says: %r' % (address, data)