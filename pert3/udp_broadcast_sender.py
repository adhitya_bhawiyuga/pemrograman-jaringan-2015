import socket, sys

# Port number
PORT = 6666
# Maximum size of UDP packet
MAX = 65535 
# Initialize socket object with UDP/Datagram type
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Set option to broadcast
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
if len(sys.argv) > 1 :
	network = sys.argv[1]
	# Send brodcast message
	s.sendto('Broadcast message!', (network, PORT))
else :
	print "use parameter address"