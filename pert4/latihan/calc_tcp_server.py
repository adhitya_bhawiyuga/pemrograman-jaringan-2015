import socket

# Port number
PORT = 7555
# Inisialisasi socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind Port
s.bind(('', PORT))
# Listen
s.listen(10)

while True:
	# Accepting incoming connection
	conn, address = s.accept()
	# Baca pesan dari client
	data, address = conn.recvfrom(4096)
	arr = data.split()
	angka1 = int(arr[1])
	angka2 = int(arr[2])
	operator = arr[0]
	hasil = 0
	if operator == "+" :
		hasil = angka1 + angka2
	elif operator == "-" :
		hasil = angka1 - angka2
	#print "Client mengirim data", data
	# Kirim balik ke client
	conn.sendall(str(hasil))	
conn.close()