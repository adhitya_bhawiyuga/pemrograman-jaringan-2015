import socket

# Port number
PORT = 6666
# Inisialisasi socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind Port
s.bind(('', PORT))
# Listen
s.listen(10)

while True:
	# Accepting incoming connection
	conn, address = s.accept()
	# Baca pesan dari client
	data, address = conn.recvfrom(4096)
	print "Client mengirim data", data
	balik = data[::-1]
	# Kirim balik ke client
	conn.sendall(balik)	
conn.close()