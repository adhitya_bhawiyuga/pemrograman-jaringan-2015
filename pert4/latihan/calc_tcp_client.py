import socket

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number
PORT = 7555
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# connect to server
s.connect((SERVER_IP, PORT))

angka1 = raw_input("Masukkan angka pertama : ")
angka2 = raw_input("Masukkan angka kedua : ")
operasi = raw_input("Masukkan operasi : ")
pesan = operasi+" "+angka1+" "+angka2+" "
# Kirim
s.sendall(pesan)
# Baca kiriman balik dari Server
data, address = s.recvfrom(4096)
print 'Server mengirim data', data
s.close()