import socket, sys
from thread import start_new_thread

from os import listdir
from os.path import isfile, join

mypath = "./share/"
# Maximum size of UDP packet
MAX = 65535 
running = True

# Port number
UDP_PORT = 6666
TCP_PORT = 6662
# Initialize socket object with UDP/Datagram type
udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Set option to broadcast
udp_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
# Bind to a certain port
udp_sock.bind(('', UDP_PORT))
# Set network address
network_addr = sys.argv[1]
# Inisialisasi socket TCP
tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcp_socket.bind(('', TCP_PORT))
tcp_socket.listen(10)

def get_shared_file_list():
	onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
	return onlyfiles

def start_tcp_server():
	global tcp_socket
	global running
	
	while running:
		# Accepting incoming connection
		conn, address = tcp_socket.accept()
		# Baca pesan dari client
		data, address = conn.recvfrom(4096)
		# Kirim balik ke client
		splitted_data = data.split('|')
		command = splitted_data[0]
		if command == "REQ" :
			filename = splitted_data[1]
			with open(join(mypath,filename), 'r') as f:
				data = f.read()
				conn.sendall(data)
		else :
			conn.send("Wrong command")
		conn.close()

def start_udp_receiver():
	global udp_sock
	global running
	global TCP_PORT
	# Read incoming message
	while running:
		data, (ip_address,port) = udp_sock.recvfrom(MAX)
		splitted_data = data.split('|')
		command = splitted_data[0]
		if command == "QUERY" :
			filename = splitted_data[1]
			file_list = get_shared_file_list()
			if filename in file_list :
				udp_sock.sendto("FOUND|"+filename, (ip_address, UDP_PORT))
		elif command == "FOUND":
			filename = splitted_data[1]
			print "\n\n-> Kirim request ke "+ip_address+" untuk mendapatkan "+filename
			# Kirim TCP request ke peer dengan ip_address
			tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			tcp_socket.connect((ip_address, TCP_PORT))
			tcp_socket.sendall('REQ|'+filename)
			file_content = tcp_socket.recv(4096)
			print "-> Isi file "+filename+" adalah :"
			print file_content
			print ""
			tcp_socket.close()

def send_udp_broadcast(data):
	udp_sock.sendto(data, (network_addr, UDP_PORT))

# Inisiasi UDP server untuk menerima broadcast
start_new_thread(start_udp_receiver ,())
# Inisiasi TCP server untuk menerima request isi file
start_new_thread(start_tcp_server ,())


while True :
	try :
		input = raw_input("Masukkan file yang akan dicari : ")
		# Send the message
		send_udp_broadcast("QUERY|"+input)
		# Start UDP receiver thread

	except KeyboardInterrupt :
		running = False
		tcp_socket.close()	
		break		
