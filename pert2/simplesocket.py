import socket,sys

sock = socket.socket()
sock.connect(('ip.jsontest.com', 80))
sock.sendall(
	'GET / HTTP/1.1\r\n'
	'Host: ip.jsontest.com:80\r\n'
	'User-Agent: socket1.py\r\n'
	'Connection: close\r\n'
	'\r\n')
# Read all received message
while True:
    buf = sock.recv(1000)
    if not buf:
        break
    print buf
sock.close()
