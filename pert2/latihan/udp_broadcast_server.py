import socket

# Port number
PORT = 6666
# Maximum size of UDP
MAX = 65535
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

# Bind 
s.bind( ('', PORT) )

while True :
	# Baca data dari client
	data, address = s.recvfrom(MAX)
	print "Client mengirimkan pesan", data