import socket

# Port number
PORT = 6666
# Maximum size of UDP
MAX = 65535
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind 
s.bind( ('', PORT) )

while True :
	# Baca data dari client
	data, address = s.recvfrom(MAX)
	print "Client mengirimkan pesan", data
	tanggal = ""
	if data == "yesterday" :
		tanggal = "11-03-2015"
	elif data == "tomorrow" :
		tanggal = "13-03-2015"
	else :
		tanggal = "12-03-2015"
	# Kembalikan data ke client
	s.sendto(tanggal, address)	