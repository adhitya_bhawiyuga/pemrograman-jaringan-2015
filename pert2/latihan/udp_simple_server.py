import socket

# Port number
PORT = 6666
# Maximum size of UDP
MAX = 65535
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind 
s.bind( ('', PORT) )

while True :
	# Baca data dari client
	data, address = s.recvfrom(MAX)
	print "Client mengirimkan pesan", data
	# Kembalikan data ke client
	s.sendto("OK "+data, address)	