import socket

BROADCAST_IP = '172.30.127.255'

PORT = 6666

MAX = 65535

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

input_user = raw_input('Masukkan inputnya : ')
# Kirim message ke server
s.sendto(input_user, (BROADCAST_IP, PORT))