import socket
import json

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number
PORT = 6666
# Inisialisasi object socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Inisialisasi koneksi
s.connect( (SERVER_IP,PORT) )

data_mahasiswa = {"nama" : "Adhitya", "nim" : "12345"}
data_siap_kirim = json.dumps(data_mahasiswa)
print data_siap_kirim
s.sendall(data_siap_kirim)