import socket

PORT = 6666

MAX = 65535
#Inisialisasi socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Bind IP dan PORT
s.bind( ('',PORT) )

while True :
	# Baca pesan dari client
	data, address = s.recvfrom(MAX)
	print "Client mengirim pesan ", data
	# Kirim balik pesannya ke client
	s.sendto("OK "+data, address)