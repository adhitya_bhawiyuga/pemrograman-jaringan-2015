import socket

PORT = 6666

#Inisialisasi socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind IP dan PORT
s.bind( ('',PORT) )
# Listen
s.listen(10)

while True :
	# Accept koneksi
	conn, address = s.accept()
	# Baca pesan dari client
	data, address = conn.recvfrom(4096)
	print "Client mengirim pesan ", data
	# Kirim balik pesannya ke client
	conn.sendall("OK "+data)