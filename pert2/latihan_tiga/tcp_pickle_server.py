import socket
import pickle

class Mahasiswa(object) :
	nama = ""
	nim = ""

	def __init__(self, namaMhs, nimMhs) :
		self.nama = namaMhs
		self.nim = nimMhs

PORT = 6666

#Inisialisasi socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind IP dan PORT
s.bind( ('',PORT) )
# Listen
s.listen(10)

while True :
	# Accept koneksi
	conn, address = s.accept()
	# Baca pesan dari client
	data, address = conn.recvfrom(4096)
	data_mahasiswa = pickle.loads(data)
	print "Nama mahasiswa ", data_mahasiswa.nama, " NIM ", data_mahasiswa.nim