#!flask/bin/python
from flask import Flask, request, render_template

app = Flask(__name__)

# Handle request GET ke alamat /
@app.route('/webcam', methods=['GET'])
def webcam():
	# Render template yang ada di direktori 'templates' dengan parameter username
	return render_template('webcam.html')

@app.route('/mic', methods=['GET'])
def mic():
	# Render template yang ada di direktori 'templates' dengan parameter username
	return render_template('mic.html')

@app.route('/room/<int:room_id>', methods=['GET'])
def room(room_id):
	# Render template yang ada di direktori 'templates' dengan parameter username
	return render_template('room.html',room_id = room_id)

if __name__ == '__main__':
	app.run(host= '0.0.0.0',debug=True, port=5000)