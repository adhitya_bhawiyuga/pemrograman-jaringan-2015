import socket, sys
import struct

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number used by server
PORT = 9999

# Format binary data tipe integer untuk kirim/terima ukuran data
format = struct.Struct('I')

def main(argv):
	# Initialize socket object with TCP/Stream type
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print 'Address before sending:', s.getsockname()
	# Initiate a CONNECTION
	s.connect((SERVER_IP, PORT))
	# Send the message
	s.sendall(format.pack(53000000))
	# Close connection
	s.close()

if __name__ == "__main__":
	main(sys.argv)