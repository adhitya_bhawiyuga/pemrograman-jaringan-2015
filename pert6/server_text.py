import socket, sys
from thread import start_new_thread

# Port number
PORT = 9999

def clientthread(conn):  
	#infinite loop so that function do not terminate and thread do not end.
	while True:		
		try :
			# Read the message stream from client
			data = conn.recv(4096)
			# Check if receive data is not empty
			if data :
				print 'Data diterima ', repr(data), ' ukuran ', sys.getsizeof(data), ' bytes'
			# Empty string means connection closed
			else :
				break			 
		except socket.error, e :
			break   
	#came out of loop
	print "Connection closed by client"
	conn.close()

def main(argv):
	# Initialize socket object with TCP/Stream type
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# Bind to a certain IP and PORT use '' to accept incoming packet from anywhere
	s.bind(('', PORT))
	# Listen the incoming connection
	s.listen(10)
	print 'Listening at', s.getsockname()
	while True:
		try :
			# Accept connection, return client socket and address
			conn, addr = s.accept()
			# Read the message stream from client with specific buffer size
			start_new_thread(clientthread ,(conn,))
		except KeyboardInterrupt :
			break
	s.close()	

if __name__ == "__main__":
	main(sys.argv)