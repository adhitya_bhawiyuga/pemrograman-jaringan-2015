import socket, sys
from thread import start_new_thread

# Port number
PORT = 9999

# Method for receiving stream of data
def receive(sock):
	data = ''
	while True:
		# Receive message
	    buf = sock.recv(5)	
	    # If stream contains termination character, return    
	    if "\r\n" in buf:
	    	buf = buf.replace("\r\n", "")
	    	data += buf
	        return data
	    # Append data
	    data += buf	    
	return data

# Method for sending stream of data
def send(sock, message):
	# Send data with termination character
	sock.sendall(message + '\r\n')

def clientthread(conn):  
	#infinite loop so that function do not terminate and thread do not end.
	while True:		
		try :			
			# Read the message stream from client
			data = receive(conn)
			# Check if receive data is not empty
			if data :				
				#data = conn.recv(4096)
				print 'The client says', repr(data)
				# Send back the message to client
				#conn.sendall('OK '+data+"\r\n")
				send(conn, 'OK '+data)
			# Empty string means connection closed
			else :
				break			 
		except socket.error, e :
			break   
	#came out of loop
	print "Connection closed by client"
	conn.close()

def main(argv):
	# Initialize socket object with TCP/Stream type
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# Bind to a certain IP and PORT use '' to accept incoming packet from anywhere
	s.bind(('', PORT))
	# Listen the incoming connection
	s.listen(10)
	print 'Listening at', s.getsockname()
	while True:
		try :
			# Accept connection, return client socket and address
			conn, addr = s.accept()
			# Read the message stream from client with specific buffer size
			start_new_thread(clientthread ,(conn,))
		except KeyboardInterrupt :
			break
	s.close()

if __name__ == "__main__":
	main(sys.argv)