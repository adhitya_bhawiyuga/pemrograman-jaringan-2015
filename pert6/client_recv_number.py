import socket, sys
import struct
from server_recv_number import receive, send

# Server IP address
SERVER_IP = '127.0.0.1'
# Port number used by server
PORT = 9999

def main(argv):
	# Initialize socket object with TCP/Stream type
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print 'Address before sending:', s.getsockname()
	# Initiate a CONNECTION
	s.connect((SERVER_IP, PORT))
	# Send the message
	send(s, 'Selamat pagi semua')
	print 'Address after sending', s.getsockname()
	# Read message stream from server 
	data = receive(s)
	print 'The server says', repr(data)
	s.close()

if __name__ == "__main__":
	main(sys.argv)