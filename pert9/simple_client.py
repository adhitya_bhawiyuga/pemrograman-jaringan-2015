import httplib, urllib
import json

conn = httplib.HTTPConnection("localhost:5000")

# Data seluruh pegawai
def semua():
	conn.request("GET", "/api/v1.0/pegawai")
	response = conn.getresponse()
	resp = response.read()
	print resp
	data = json.loads(resp)
	peg = data["pegawai"]
	for p in peg :
		print p['nama'], p['alamat']
	#print response.read()

# Data pegawai dengan ID
def pegawai(id) :	
	conn.request("GET", "/api/v1.0/pegawai/"+str(id))
	response = conn.getresponse()
	data = json.loads(response.read())
	peg = data["pegawai"]
	print peg['nama'], peg['alamat']
	#print response.read()

def tambah_pegawai(nama="", alamat=""):
	headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
	params = urllib.urlencode({'nama': nama, 'alamat' : alamat})
	conn.request("POST", "/api/v1.0/pegawai", params, headers)
	response = conn.getresponse()
	print response.read()

def update_pegawai(id, nama="", alamat=""):
	headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
	params = urllib.urlencode({'nama': nama, 'alamat' : alamat})
	conn.request("PUT", "/api/v1.0/pegawai/"+str(id), params, headers)
	response = conn.getresponse()
	print response.read()

def delete_pegawai(id):
	conn.request("DELETE", "/api/v1.0/pegawai/"+str(id))
	response = conn.getresponse()
	print response.read()

semua()
#pegawai(1)
#tambah_pegawai('Parjo', 'Kediri')
#update_pegawai(5, 'Parjo2', 'Kediri')
#delete_pegawai(5)