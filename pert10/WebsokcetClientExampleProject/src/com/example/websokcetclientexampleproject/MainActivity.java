package com.example.websokcetclientexampleproject;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;


public class MainActivity extends Activity {
    //private WebSocketClient mWebSocketClient;
    String TAG="websocket";
    static Button buttonSend;
	static Button buttonConnect;
	static EditText textAddress;
	boolean isConnected=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //connectWebSocket();

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            buttonSend = (Button) rootView.findViewById(R.id.buttonSend);
            buttonConnect = (Button) rootView.findViewById(R.id.buttonConnect);
            textAddress = (EditText) rootView.findViewById(R.id.textAddress);

            return rootView;
        }
    }
    private final WebSocketConnection mConnection = new WebSocketConnection();
    private void connectWebSocket() {
	   //final String wsuri = "ws://192.168.1.6:9999";
    	
       final String wsuri = textAddress.getText().toString();
	   try {
		   	mConnection.connect(wsuri, new WebSocketHandler() {

	         @Override
	         public void onOpen() {
	        	 buttonSend.setEnabled(true);
	        	 buttonConnect.setText("Disconnect");
	        	 isConnected = true;
	         }

	         @Override
	         public void onTextMessage(String payload) {		            
	            final String message = payload;
	            runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView textView = (TextView)findViewById(R.id.messages);
                        textView.setText(textView.getText() + "\n" + message);
                    }
                });
	         }

	         @Override
	         public void onClose(int code, String reason) {
	        	 buttonSend.setEnabled(false);
	        	 buttonConnect.setText("Connect");
	        	 isConnected = false;
	         }
	      });
	   } catch (WebSocketException e) {
	      Log.d(TAG, e.toString());
	   }
    }
    
    public void sendMessage(View view) {
        EditText editText = (EditText)findViewById(R.id.message);
        mConnection.sendTextMessage(editText.getText().toString());
        editText.setText("");
    }
    public void connectToServer(View view) {
    	if(!isConnected){
    		connectWebSocket();
    	}
    	else {
    		mConnection.disconnect();
    	}
    }
}
