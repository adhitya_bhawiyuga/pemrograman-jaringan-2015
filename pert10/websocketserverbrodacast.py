# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.
from twisted.internet import reactor, protocol, error
from txws import WebSocketFactory
from twisted.python import failure

clients = list()

connectionDone=failure.Failure(error.ConnectionDone())
connectionDone.cleanFailure()

class Echo(protocol.Protocol):
	"""This is just about the simplest possible protocol"""
	
	def dataReceived(self, data):
		print "Receive : "+data
		# Kirim data untuk setiap koneksi yang ada
		for cl in clients :
			cl.transport.write("Broadcast "+data)

	def connectionMade(self):
		# Simpan koneksi client ke sebuah list
		clients.append(self)

	def connectionLost(self, reason=connectionDone):
		# Hapus koneksi dari list jika disconnected
		clients.remove(self)


def main():
	"""This runs the protocol on port 8000"""
	factory = protocol.ServerFactory()
	factory.protocol = Echo
	reactor.listenTCP(9999,WebSocketFactory(factory))
	reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
	main()
